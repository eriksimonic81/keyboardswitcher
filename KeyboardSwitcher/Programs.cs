﻿using Microsoft.Win32;
using System;
using System.Linq;
using System.Windows.Forms;

namespace SimpleKeyboadChanger
{
    public partial class Programs : Form
    {


        RegistryKey rkApp;

        private const string regKey = "KeyboardSwitcherHellper";

        public Programs(string currentProgram, Action a)
        {
            InitializeComponent();
            InitControls(currentProgram, a);
        }


        private void InitControls(string currentProgram, Action a)
        {
            rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            // The value doesn't exist, the application is not set to run at startup
            var v = rkApp.GetValue(regKey);
            checkBox1.Checked = v != null && Application.ExecutablePath.ToString() == v.ToString();
            

            checkBox1.Click += delegate
            {
                if (checkBox1.Checked)
                {
                    rkApp.SetValue(regKey, Application.ExecutablePath.ToString());
                }
                else
                {
                    rkApp.DeleteValue(regKey, false);
                }
            };

            this.FormClosing += delegate
            {
                a.Invoke();
            };



            this.Icon = Properties.Resources.Keyboard;
            this.StartPosition = FormStartPosition.CenterScreen;

            tb_Item.Text = currentProgram;

            Action Rebind = () =>
            {
                lb_Items.DataSource = null;
                lb_Items.DataSource = Properties.Settings.Default.Applications;
            };
            btn_Close.Click += delegate
            {
                this.Close();
            };

            button1.Enabled = false;
            button1.Click += delegate
            {
                if (MessageBox.Show(lb_Items.SelectedItem.ToString(), "Delete?", MessageBoxButtons.YesNo) ==
                    System.Windows.Forms.DialogResult.Yes)
                {
                    Properties.Settings.Default.Applications.RemoveAt(lb_Items.SelectedIndex);
                    Properties.Settings.Default.Save();
                    Rebind.Invoke();
                }
            };

            lb_Items.SelectedIndexChanged += delegate
            {
                button1.Enabled = lb_Items.SelectedIndex >= 0;
            };

            lb_Items.MouseDoubleClick += (s, e) =>
            {
                if (null != lb_Items.SelectedItem)
                {
                    tb_Item.Text = lb_Items.SelectedItem.ToString();
                    Rebind.Invoke();
                }
            };

            btn_Save.Click += delegate
            {
                Properties.Settings.Default.Applications.Clear();
                Properties.Settings.Default.Applications.AddRange(lb_Items.Items.Cast<string>().ToArray());
                Properties.Settings.Default.Save();
                this.Close();
                //Rebind.Invoke();
            };

            btn_Update.Click += delegate
            {
                if (!string.IsNullOrEmpty(tb_Item.Text))
                {
                    if (!Properties.Settings.Default.Applications.Contains(tb_Item.Text))
                    {
                        Properties.Settings.Default.Applications.Add(tb_Item.Text);
                        Properties.Settings.Default.Save();
                        Rebind.Invoke();
                    }
                }
                tb_Item.Text = "";
            };

            lb_Items.DataSource = Properties.Settings.Default.Applications;

            var i = lb_Items.FindString(currentProgram);
            if (i >= 0)
            {
                lb_Items.SelectedIndex = i;
            }
        }
    }
}