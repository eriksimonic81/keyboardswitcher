﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SimpleKeyboadChanger
{
    internal class Program
    {
        private static string currentProgram = string.Empty;

        private static string[] _SIPrograms = null;

        private static string[] SIPrograms
        {
            get
            {
                if (null == _SIPrograms)
                {
                    _SIPrograms = new string[Properties.Settings.Default.Applications.Count];
                    Properties.Settings.Default.Applications.CopyTo(_SIPrograms, 0);
                }
                return _SIPrograms;
            }
        }

        private static string getLocaleId(string p)
        {
            return (SIPrograms.Contains(p)) ? secondaryLayout : primaryLayout;
        }

        [STAThread]
        private static void Main(string[] args)
        {


            primaryLayout = Properties.Settings.Default.global_keyboard;
            secondaryLayout = Properties.Settings.Default.specific_keyboard;

            Timer t = new Timer();
            t.Interval = 1500;
            t.Tick += delegate
            {
                try
                {
                    IntPtr hWnd = GetForegroundWindow();
                    if (hWnd != curentHWD)
                    {
                        uint procId = 0;
                        GetWindowThreadProcessId(hWnd, out procId);
                        var procnameCl = Process.GetProcessById((int)procId).MainModule;
                        var procname = procnameCl.ModuleName.ToString().ToLower();
                        if (procname != currentProgram)
                            ChangeLanguage(getLocaleId(procname));

                        currentProgram = procname;
                        curentHWD = hWnd;
                    }
                }
                catch { }
            };
            t.Start();
            var n = new NotifyIcon();
            
            n.ContextMenu = new ContextMenu(new MenuItem[] {
                new MenuItem(){Text = "Configure"},
                new MenuItem(){Text = "Exit"}
            });
            

            n.Icon = Properties.Resources.Keyboard;

            Action showProgramConfigWindow = () => {

                if (_selector == null)
                {
                    _selector = new Programs(currentProgram, () =>
                    {
                        _selector = null;
                        _SIPrograms = null;
                    });
                    _selector.Show();
                }
            };

            n.ContextMenu.MenuItems[0].Click += delegate
            {
                showProgramConfigWindow.Invoke();
            };
            n.ContextMenu.MenuItems[1].Click += delegate
            {
                n.Visible = false;
                t.Stop();
                Application.Exit();
            };
            n.Visible = true;

            n.Click += delegate
            {
                showProgramConfigWindow.Invoke();
            };
            Application.Run();
        }

        [DllImport("user32.dll")]
        private static extern bool PostMessage(int hhwnd, uint msg, IntPtr wparam, IntPtr lparam);

        [DllImport("user32.dll")]
        private static extern IntPtr LoadKeyboardLayout(string pwszKLID, uint Flags);

        private static uint WM_INPUTLANGCHANGEREQUEST = 0x0050;
        private static int HWND_BROADCAST = 0xffff;
        private static string primaryLayout = "00000409";
        private static string secondaryLayout = "00000424";

        private static uint KLF_ACTIVATE = 1;

        private static void ChangeLanguage(string locale)
        {
            PostMessage(HWND_BROADCAST, WM_INPUTLANGCHANGEREQUEST, IntPtr.Zero, LoadKeyboardLayout(locale, KLF_ACTIVATE));
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        public static IntPtr curentHWD { get; set; }

        public static Programs _selector { get; set; }
    }
}